package de.simonknott.owniam.users.core.models;

import javax.persistence.*;

@Entity
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;

  private String firstName;
  private String lastName;

  @Column(unique=true)
  private String username;

  @Column(unique=true)
  private String emailAddress;

  private String hashedPassword;

}
