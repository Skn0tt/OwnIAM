package de.simonknott.owniam.users;

import de.simonknott.owniam.users.core.models.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

  UserRepository repository;

  @GetMapping("/")
  Iterable<User> getAllUsers() {
    return repository.findAll();
  }


}
