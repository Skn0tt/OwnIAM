package de.simonknott.owniam.users;

import de.simonknott.owniam.users.core.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {

}
