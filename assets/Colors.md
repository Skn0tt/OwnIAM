# Colour-Scheme

<style>
.square {
  display: inline-block;
  width: 15px;
  height: 15px;
  border: 1px solid rgba(0, 0, 0, .2);
}

.p {
  background: #ffb300;
}

.pl {
  background: #ffe54c;
}

.pd {
  background: #c68400;
}

.s {
  background: #00e676;
}

.sl {
  background: #66ffa6;
}

.sd {
  background: #00b248;
}
</style>

## Primary

Normal: #ffb300 <span class="square p"/>
Light: #ffe54c <span class="square pl"/>
Light: #c68400 <span class="square pd"/>

## Secondary

Normal: #00e676 <span class="square s"/>
Light: #66ffa6 <span class="square sl"/>
Light: #00b248 <span class="square sd"/>
