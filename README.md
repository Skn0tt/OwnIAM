<p align="center">
  <img alt="OwnIAM" src="./assets/Logo.png" width="320"/>
</p>

<h1 align="center">
  OwnIAM
</h1>

<p align="center">
  A Cloud-Native, Selfhosted IAM.
</p>

<p align="center">
</p>

- [Documentation](https://skn0tt.gitlab.io/OwnIAM/)
- [Contributing](https://skn0tt.gitlab.io/OwnIAM/Contributing)

> OwnIAM is currently under active construction.
> Don't hesitate to contribute!
