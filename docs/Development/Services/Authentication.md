# Authentication

`Authentication` checks authentication attempts and issues auth tokens.

```plantuml
@startuml


cloud {
  [RailMail]
  [Signer]
}

[Users]

package "Authentication" as package {

  component Authentication [
    Authentication
  ]

  database Redis

  [Authentication] -> Postgres

}

[Authentication] .up.> Signer
[Authentication] .up.> RailMail
[Authentication] -up-> Users

[Authentication] -- REST : provides

@enduml
```

It provides a REST interface.
