# OpenID

`OpenID` implements the [*OpenID Connect*](http://openid.net/connect/) Protocol and enables federated auth for other services.

```plantuml
@startuml

package "OpenID" as package {

  component OpenID [
    OpenID
  ]

  database Redis

  [OpenID] -> Redis

}

OpenID -up-> [Authentication]
OpenID <-up-> [UI]

interface "OpenID Connect" as iOID

[OpenID] -- iOID : provides

@enduml
```
