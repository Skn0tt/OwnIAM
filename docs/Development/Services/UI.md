# UI

UI serves the client SPA to the user.

```plantuml
@startuml

actor Client

package UI as package {
  [UI]
}

UI ..> [Users]
UI ..> [Authentication]
UI ..> [OpenID]
UI ..> [PasswordReset]
UI ..> [SignUp]

Client -- UI

@enduml
```

The SPA is implemented in Elm and served by Nginx.
