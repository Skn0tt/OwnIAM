# PasswordReset

`PasswordReset` manages the password-reset flow.

```plantuml
@startuml
package "PasswordReset" as package {

  component PasswordReset [
    PasswordReset
  ]

  database Redis

  [PasswordReset] -> Redis

}

cloud {
  [RailMail]
}

PasswordReset .up.> RailMail
PasswordReset -up-> [Users]

[PasswordReset] -- REST : provides

@enduml
```

It provides a REST interface.
