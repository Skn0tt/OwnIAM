# SignUp

`SignUp` manages user signup.

```plantuml
@startuml
package "SignUp" as package {

  component SignUp [
    SignUp
  ]

  database Postgres

  [SignUp] -> Postgres

}

[SignUp] -- REST : provides

@enduml
```

It provides a REST interface to interact with the data.
