# Users

`Users` manages all users and their data.

```plantuml
@startuml
package "Users" as package {

  component Users [
    Users
  ]

  database Postgres
  database Minio

  [Users] -left-> Postgres
  [Users] -right-> Minio

}

[Users] -- REST : provides

@enduml
```

It provides a REST interface to interact with the data.
