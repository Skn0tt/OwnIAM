# Architecture

OwnIAM is powered by a microservices architecture.
This enables it to scale horizontally and handle dynamic request loads.

## High-Level Structure

```plantuml
@startuml
skinparam componentStyle uml2

[Authentication]
[SignUp]
[PasswordReset]
[Users]
[OpenID]
[UI]

[Authentication] --> [Users]
[SignUp] --> [Users]
[PasswordReset] --> [Users]
[OpenID] --> [Authentication]
[UI] --> [Users]
[UI] --> [Authentication]
[UI] --> [SignUp]
[UI] --> [PasswordReset]
[UI] <-> [OpenID]

@enduml
```

A more detailed System Diagram which includes infrastructure services:

```plantuml
@startuml
skinparam componentStyle uml2

[Authentication]
[SignUp]
[PasswordReset]
[Users]
[OpenID]
[UI]

cloud {
  [Signer]
  [RailMail]
}

' Internal

[Authentication] --> [Users]
[SignUp] --> [Users]

[PasswordReset] --> [Users]

[OpenID] --> [Authentication]
[UI] --> [Users]
[UI] --> [Authentication]
[UI] --> [SignUp]
[UI] --> [PasswordReset]
[UI] <-> [OpenID]

note left of [UI]
  Serves to Browsers
end note

note right of [OpenID]
  implements "OpenID Connect"
end note

' Infrastructure

[SignUp] ..> [RailMail]
[PasswordReset] ..> [RailMail]
[Authentication] ..> [Signer]
[Authentication] ..> [RailMail]

note bottom of [RailMail]
  GitHub: skn0tt/RailMail
end note

note bottom of [Signer]
  GitHub: skn0tt/Signer
end note

@enduml
```

## Services

- [Users](Services/Users.md)


