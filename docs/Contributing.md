# Contributing to OwnIAM

👍🎉 First off, thanks for taking the time to contribute! 🎉👍

## How to Contribute

First, create an issue stating the problem and wait for an answer so you can be sure nobody else works on the same issue.
Then, fork the Repo and make your fixes.
After testing them (consider implementing Unit- or E2E-Tests, for Bug-Fixes a Regression Test would be great), create a merge request.
A contributor will happily review and then merge the request!

## What Should I Know Before I Get Started?

Make sure to read about OwnIAM's internals in the [Architecture Docs](./Development/Architecture.md) to figure out where you want to contribute.
Then proceed by reading about it in its respective docs.
